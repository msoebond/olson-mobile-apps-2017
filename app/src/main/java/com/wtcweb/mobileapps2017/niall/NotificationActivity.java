package com.wtcweb.mobileapps2017.niall;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.wtcweb.mobileapps2017.R;

public class NotificationActivity extends AppCompatActivity {

    EditText txtNotificationMessage;
    Button btnNotification;
    Button btnNotificationWithButton;

    // Just testing forking and pull requests

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        // get a handle on the widgets in the layout
        txtNotificationMessage = (EditText)findViewById(R.id.txtNotificationMessage);
        btnNotification = (Button)findViewById(R.id.btnNotification);
        btnNotificationWithButton = (Button)findViewById(R.id.btnNotificationWithButton);

        // hook up the event listeners to the buttons
        btnNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = txtNotificationMessage.getText().toString();
                sendNotification(message); // this will create a notification without a button in it
                Toast.makeText(NotificationActivity.this, "Look in your notification bar!!!", Toast.LENGTH_LONG).show();
            }
        });

        btnNotificationWithButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = txtNotificationMessage.getText().toString();
                sendNotificationWithButon(message); // this one will have a button in the notifcation
                Toast.makeText(NotificationActivity.this, "Look in your notification bar!!!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void sendNotification(String message){

        // Build a Notification (this makes heavy use of Method Chaining)
        Notification notification = new Notification.Builder(this)
                .setContentTitle("Ha! We created a Notification!")
                .setContentText(message)
                .setSmallIcon(android.R.drawable.ic_notification_overlay)    //This is the icon that will appear in the notification, it is one of the icons that comes with Androd
                .build();

        // hide the notification after its selected (
        // the default is for it to stay visible at the top of the phone until the user manually clears it)
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // this will make the notification appear (you have to use the NotificationManager to make it appear at the top of the phone screen)....
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
    }

    private void sendNotificationWithButon(String message){

        //HERE'S A NOTIFICATION THAT INCLUDES A BUTTON (WHEN THE BUTTON IS PRESSED, IT WILL LAUNCH AN INTENT THAT OPENS ReceiveNotificationActivity)
        // first we create the intent that will get launch when the button is pressed...
        Intent intent = new Intent(this, ReceiveNotificationActivity.class);
        intent.putExtra(ReceiveNotificationActivity.NOTIFICATION_EXTRA, message);
        PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, 0);

        // Now we create the button (aka an 'action' button) that we can add to the notification...
        // (Notice how we add the pIntent to it)
        NotificationCompat.Action action = new NotificationCompat.Action.Builder(android.R.drawable.btn_star, "Go to ReceiveNotificationActivity", pIntent).build();

        // Build a Notification (this makes heavy use of Method Chaining)
        Notification notification = new Notification.Builder(this)
                .setContentTitle("Ha! We are creating a Notification!")
                .setContentText("The sample code you are running creates a notification WITH A BUTTON!")
                .setSmallIcon(android.R.drawable.ic_notification_overlay)    //This is the icon that will appear in the notification, it is one of the icons that comes with Androd
                .addAction(android.R.drawable.btn_star, "Launch ReceiveNotificationActivity", pIntent)
                .build();

        // hide the notification after its selected (
        // the default is for it to stay visible at the top of the phone until the user manually clears it)
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // this will make the notification appear (you have to use the NotificationManager to make it appear at the top of the phone screen)....
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(0, notification);
    }
}

